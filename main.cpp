// g++ -std=c++11 -O3 -o main main.cpp -DNDEBUG
// gnuplot
// set pm3d map
// splot "fields/rho-fs-10.txt" matrix

#include <iostream>
#include <cmath>
#include <array>
#include <algorithm>

#include "../matrix/matrix.hpp"
#include "toString.hpp"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template<typename T>
T sqr(T value)
{
  return value*value;
}

double minmod(double u1, double u2)
{
  double u = 0;
  if (u1*u2 > 0) 
    if(fabs(u1) < fabs(u2)) u = u1;
    else 
      if (fabs(u2) < fabs(u1)) u = u2;
  
  return u;
}

int nx = 100;
int ny = 100;
double omega = 1;

int nSteps = 500;

double lx = 1;
double ly = 1;
double dx = lx/nx;
double dy = ly/ny;

//double dt = 0.01;
double dt = 2*M_PI/nSteps;

void init(Matrix& rho)
{


  // for(int i=0; i<u.get_nx(); i++) 
  //   for(int j=0; j<u.get_ny(); j++) {
  //     u(i,j) = 1;
  //   }

  rho = 0;

  double x0 = 0.3;
  double y0 = 0.5;
  double sigma = 0.05;

  for(int i=0; i<rho.get_nx(); i++) 
    for(int j=0; j<rho.get_ny(); j++) {
      rho(i,j) = exp(-0.5*sqr((i*dx-x0)/sigma)-0.5*sqr((j*dy-y0)/sigma));
    }



//   for(int i=0; i<rho.get_nx(); i++) 
//     for(int j=0; j<rho.get_ny(); j++) {
//       if(sqr(i*dx-x0)+sqr(j*dy-y0) < sqr(0.05))
//   	rho(i,j) = 1;
//     }

  for(int i=0; i<rho.get_nx(); i++) 
    for(int j=0; j<rho.get_ny(); j++) {
      if(i*dx < 0.8 && i*dx > 0.55 && j*dy < 0.625 && j*dy > 0.375)
	rho(i,j) += 1;

      // if(sqr(i*dx-x0)+sqr(j*dy-y0) < sqr(0.05))
      // 	rho(i,j) = 1;
    }
      

  // for(int i=0; i<rho.get_nx(); i++) 
  //   for(int j=0; j<rho.get_ny(); j++) {
  //     if(sqr(i*dx-x0)+sqr(j*dy-y0) < sqr(0.05))
  // 	rho(i,j) = 1;
  //   }

}

int main(int argc, char** argv)
{
  std::cerr << "2D advection starts\n";

  std::cout << "nx = " << nx << ", ny = " << ny << std::endl;
  std::cout << "lx = " << lx << ", ly = " << ly << std::endl;
  std::cout << "dx = " << dx << ", dy = " << dy << std::endl;

  Matrix u(nx,ny,"u");
  Matrix up(nx,ny,"up");
  Matrix um(nx,ny,"um");
  Matrix v(nx,ny,"v");
  Matrix vp(nx,ny,"vp");
  Matrix vm(nx,ny,"vm");
  Matrix rho(nx,ny,"rho");
  Matrix rho0(nx,ny,"rho0");
  Matrix rhoSplit(nx,ny,"rhoSplit");
  Matrix fi(nx,ny,"fi");
  Matrix fo(nx,ny,"fo");

  // v = 0;
      
  rho0.write("rho-0.txt");

  for(int i=0; i<u.get_nx(); i++) 
    for(int j=0; j<u.get_ny(); j++) {
      u(i,j) = -omega*(j*dy-0.5);
      v(i,j) =  omega*(i*dx-0.5);
    }

  // for(int i=0; i<u.get_nx(); i++) 
  //   for(int j=0; j<u.get_ny(); j++) {
  //     u(i,j) = 0.5;
  //     v(i,j) = 0;
  //   }

  for(int i=0; i<u.get_nx(); i++) 
    for(int j=0; j<u.get_ny(); j++) {
      up(i,j) = std::max(u(i,j),0.);
      um(i,j) = std::min(u(i,j),0.);
      vp(i,j) = std::max(v(i,j),0.);
      vm(i,j) = std::min(v(i,j),0.);
    }
  
  // donnor-cell upwind method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-dcu-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) = rho0(i,j) - dt/dx*(up(i,j)*(rho0(i,j) - rho0(i-1,j)) + um(i,j)*(rho0(i+1,j) - rho0(i,j)))
	  - dt/dx*(vp(i,j)*(rho0(i,j) - rho0(i,j-1)) + vm(i,j)*(rho0(i,j+1) - rho0(i,j)));
      }

    rho0 = rho;
  }

  // corner transport upwind method ----------------------------------------------------------------------------------------------
  
  init(rho0);

  for(int step = 0; step <= nSteps; step++) {
    
    if(step%10 == 0)
      rho0.write(toString("fields/rho-ctu-",step,".txt"));
    
    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) =  rho0(i,j) - dt/dx*(up(i,j)*(rho0(i,j) - rho0(i-1,j)) + um(i,j)*(rho0(i+1,j) - rho0(i,j)))
	  - dt/dx*(vp(i,j)*(rho0(i,j) - rho0(i,j-1)) + vm(i,j)*(rho0(i,j+1) - rho0(i,j)))
	  + 0.5*sqr(dt)*(up(i,j)/dx*vp(i,j)/dy*(rho0(i,j)     - rho0(i,j-1)    - rho0(i-1,j)   + rho0(i-1,j-1)) +
	  		 up(i,j)/dx*vm(i,j)/dy*(rho0(i,j+1)   - rho0(i,j)      - rho0(i-1,j+1) + rho0(i-1,j)) +
	  		 um(i,j)/dx*vp(i,j)/dy*(rho0(i+1,j)   - rho0(i+1,j-1)  - rho0(i,j)     + rho0(i,j-1)) +
	  		 um(i,j)/dx*vm(i,j)/dy*(rho0(i+1,j+1) - rho0(i+1,j)    - rho0(i,j+1)   + rho0(i,j)))
	  + 0.5*sqr(dt)*(vp(i,j)/dy*up(i,j)/dx*(rho0(i,j)     - rho0(i-1,j)    - rho0(i,j-1)   + rho0(i-1,j-1)) +
	  		 vp(i,j)/dy*um(i,j)/dx*(rho0(i+1,j)   - rho0(i,j)      - rho0(i+1,j-1) + rho0(i,j-1)) +
	  		 vm(i,j)/dy*up(i,j)/dx*(rho0(i,j+1)   - rho0(i-1,j+1)  - rho0(i,j)     + rho0(i-1,j)) +
	  		 vm(i,j)/dy*um(i,j)/dx*(rho0(i+1,j+1) - rho0(i,j+1)    - rho0(i+1,j)   + rho0(i,j)));
      }
    
    rho0 = rho;
  }

  // upwind split method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-us-",step,".txt"));
    
    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoSplit(i,j) = rho0(i,j) - dt/dx*(up(i,j)*(rho0(i,j) - rho0(i-1,j)) + um(i,j)*(rho0(i+1,j) - rho0(i,j)));
      }
    
    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) = rhoSplit(i,j) - dt/dx*(vp(i,j)*(rhoSplit(i,j) - rhoSplit(i,j-1)) + vm(i,j)*(rhoSplit(i,j+1) - rhoSplit(i,j)));
      }
    
    rho0 = rho;
  }

  // upwind strang split method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

  if(step%10 == 0)
     rho0.write(toString("fields/rho-uss-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoSplit(i,j) = rho0(i,j) - 0.5*dt/dx*(up(i,j)*(rho0(i,j) - rho0(i-1,j)) + um(i,j)*(rho0(i+1,j) - rho0(i,j)));
      }
    
    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho0(i,j) = rhoSplit(i,j) - dt/dx*(vp(i,j)*(rhoSplit(i,j) - rhoSplit(i,j-1)) + vm(i,j)*(rhoSplit(i,j+1) - rhoSplit(i,j)));
      }

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) = rho0(i,j) - 0.5*dt/dx*(up(i,j)*(rho0(i,j) - rho0(i-1,j)) + um(i,j)*(rho0(i+1,j) - rho0(i,j)));
      }
    
    rho0 = rho;
  }

  // lax-friedrichs method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-lf-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) = 0.25*(rho0(i+1,j)+rho0(i-1,j)+rho0(i,j+1)+rho0(i,j-1))
	  -0.5*u(i,j)*dt/dx*(rho0(i+1,j)-rho0(i-1,j))
	  -0.5*v(i,j)*dt/dy*(rho0(i,j+1)-rho0(i,j-1));
      }
    
    rho0 = rho;
  }

  // Fromm split method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-fs-",step,".txt"));

    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
    	fi(i,j) = 
    	  up(i-1,j)*(rho0(i-1,j) + 0.25*(1-fabs(dt*u(i-1,j)/dx))*(rho0(i,j)-rho0(i-2,j))) -
    	  um(i+1,j)*(rho0(i+1,j) - 0.25*(1-fabs(dt*u(i+1,j)/dx))*(rho0(i+2,j)-rho0(i,j)));
	
    	fo(i,j) = 
    	  up(i,j)*(rho0(i,j) + 0.25*(1-fabs(dt*u(i,j)/dx))*(rho0(i+1,j)-rho0(i-1,j))) -
    	  um(i,j)*(rho0(i,j) - 0.25*(1-fabs(dt*u(i,j)/dx))*(rho0(i+1,j)-rho0(i-1,j)));
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dx*(fi(i,j) - fo(i,j));
      }    

    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
    	fi(i,j) = 
    	  vp(i,j-1)*(rho0(i,j-1) + 0.25*(1-fabs(dt*v(i,j-1)/dy))*(rho0(i,j)-rho0(i,j-2))) -
    	  vm(i,j+1)*(rho0(i,j+1) - 0.25*(1-fabs(dt*v(i,j+1)/dy))*(rho0(i,j+2)-rho0(i,j)));
	
    	fo(i,j) = 
    	  vp(i,j)*(rho0(i,j) + 0.25*(1-fabs(dt*v(i,j)/dy))*(rho0(i,j+1)-rho0(i,j-1))) -
    	  vm(i,j)*(rho0(i,j) - 0.25*(1-fabs(dt*v(i,j)/dy))*(rho0(i,j+1)-rho0(i,j-1)));
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dy*(fi(i,j) - fo(i,j));
      }    
  }

  // Fromm improved split method ----------------------------------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-fis-",step,".txt"));
    
    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
    	fi(i,j) = 
    	  0.5*(up(i,j)+up(i-1,j))*(rho0(i-1,j) + 0.25*(1-fabs(dt*0.5*(up(i,j)+up(i-1,j))/dx))*(rho0(i,j)-rho0(i-2,j))) -
    	  0.5*(um(i+1,j)+um(i,j))*(rho0(i+1,j) - 0.25*(1-fabs(dt*0.5*(um(i+1,j)+um(i,j))/dx))*(rho0(i+2,j)-rho0(i,j)));
	
    	fo(i,j) = 
    	  0.5*(up(i+1,j)+up(i,j))*(rho0(i,j) + 0.25*(1-fabs(dt*0.5*(up(i+1,j)+up(i,j))/dx))*(rho0(i+1,j)-rho0(i-1,j))) -
    	  0.5*(um(i,j)+um(i-1,j))*(rho0(i,j) - 0.25*(1-fabs(dt*0.5*(um(i,j)+um(i-1,j))/dx))*(rho0(i+1,j)-rho0(i-1,j)));
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dx*(fi(i,j) - fo(i,j));
      }    

    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
    	fi(i,j) = 
    	  0.5*(vp(i,j)+vp(i,j-1))*(rho0(i,j-1) + 0.25*(1-fabs(dt*0.5*(vp(i,j)+vp(i,j-1))/dy))*(rho0(i,j)-rho0(i,j-2))) -
    	  0.5*(vm(i,j+1)+vm(i,j))*(rho0(i,j+1) - 0.25*(1-fabs(dt*0.5*(vm(i,j+1)+vm(i,j))/dy))*(rho0(i,j+2)-rho0(i,j)));
	
    	fo(i,j) = 
    	  0.5*(vp(i,j+1)+vp(i,j))*(rho0(i,j) + 0.25*(1-fabs(dt*0.5*(vp(i,j+1)+vp(i,j))/dy))*(rho0(i,j+1)-rho0(i,j-1))) -
    	  0.5*(vm(i,j)+vm(i,j-1))*(rho0(i,j) - 0.25*(1-fabs(dt*0.5*(vm(i,j)+vm(i,j-1))/dy))*(rho0(i,j+1)-rho0(i,j-1)));
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dy*(fi(i,j) - fo(i,j));
      }    
  }

  // Van Leer split method ----------------------------------------------------------------------------------------------

  init(rho0);
  
  Matrix phi(nx,ny,"phi");
  
  for(int step = 0; step <= nSteps; step++) {
    
    if(step%10 == 0)
      rho0.write(toString("fields/rho-vl-",step,".txt"));
    
    std::array<double,3> slopeVec;
    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
	double phi = sgn((rho0(i,j)-rho0(i-1,j))*(rho0(i-1,j)-rho0(i-2,j)));
	slopeVec[0] = 2*fabs(rho0(i,j)-rho0(i-1,j));
	slopeVec[1] = 2*fabs(rho0(i-1,j)-rho0(i-2,j));
	slopeVec[2] = 0.5*fabs(rho0(i,j)-rho0(i-2,j));
	double slope = 0;
	if(phi == 1)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i,j)-rho0(i-2,j));
	
	fi(i,j) = up(i,j)*(rho0(i-1,j) + 0.5*(1-dt*fabs(up(i,j))/dx)*slope);
	
	phi = sgn((rho0(i+2,j)-rho0(i+1,j))*(rho0(i+1,j)-rho0(i,j)));
	slopeVec[0] = 2*fabs(rho0(i+1,j)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i+2,j)-rho0(i+1,j));
	slopeVec[2] = 0.5*fabs(rho0(i+2,j)-rho0(i,j));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i+2,j)-rho0(i,j));

	fi(i,j) -= um(i,j)*(rho0(i+1,j) - 0.5*(1-dt*fabs(um(i,j))/dx)*slope);

	phi = sgn((rho0(i+1,j)-rho0(i,j))*(rho0(i,j)-rho0(i-1,j)));
	slopeVec[0] = 2*fabs(rho0(i+1,j)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i,j)-rho0(i-1,j));
	slopeVec[2] = 0.5*fabs(rho0(i+1,j)-rho0(i-1,j));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i+1,j)-rho0(i-1,j));

	fo(i,j) = up(i,j)*(rho0(i,j) + 0.5*(1-dt*fabs(up(i,j))/dx)*slope);

	phi = sgn((rho0(i+1,j)-rho0(i,j))*(rho0(i,j)-rho0(i-1,j)));
	slopeVec[0] = 2*fabs(rho0(i+1,j)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i,j)-rho0(i-1,j));
	slopeVec[2] = 0.5*fabs(rho0(i+1,j)-rho0(i-1,j));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i+1,j)-rho0(i-1,j));

	fo(i,j) -= um(i,j)*(rho0(i,j) - 0.5*(1-dt*fabs(um(i,j))/dx)*slope);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dx*(fi(i,j) - fo(i,j));
      }

    // v update -----------------------------------------------------------------------------------

    for(int i=2; i<rho.get_nx()-2; i++) 
      for(int j=2; j<rho.get_ny()-2; j++) {
	double phi = sgn((rho0(i,j)-rho0(i,j-1))*(rho0(i,j-1)-rho0(i,j-2)));
	slopeVec[0] = 2*fabs(rho0(i,j)-rho0(i,j-1));
	slopeVec[1] = 2*fabs(rho0(i,j-1)-rho0(i,j-2));
	slopeVec[2] = 0.5*fabs(rho0(i,j)-rho0(i,j-2));
	double slope = 0;
	if(phi == 1)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i,j)-rho0(i,j-2));
	
	fi(i,j) = vp(i,j)*(rho0(i,j-1) + 0.5*(1-dt*fabs(vp(i,j))/dx)*slope);
	
	phi = sgn((rho0(i,j+2)-rho0(i,j+1))*(rho0(i,j+1)-rho0(i,j)));
	slopeVec[0] = 2*fabs(rho0(i,j+1)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i,j+2)-rho0(i,j+1));
	slopeVec[2] = 0.5*fabs(rho0(i,j+2)-rho0(i,j));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i,j+2)-rho0(i,j));

	fi(i,j) -= vm(i,j)*(rho0(i,j+1) - 0.5*(1-dt*fabs(vm(i,j))/dx)*slope);

	phi = sgn((rho0(i,j+1)-rho0(i,j))*(rho0(i,j)-rho0(i,j-1)));
	slopeVec[0] = 2*fabs(rho0(i,j+1)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i,j)-rho0(i,j-1));
	slopeVec[2] = 0.5*fabs(rho0(i,j+1)-rho0(i,j-1));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i,j+1)-rho0(i,j-1));

	fo(i,j) = vp(i,j)*(rho0(i,j) + 0.5*(1-dt*fabs(vp(i,j))/dx)*slope);

	phi = sgn((rho0(i,j+1)-rho0(i,j))*(rho0(i,j)-rho0(i,j-1)));
	slopeVec[0] = 2*fabs(rho0(i,j+1)-rho0(i,j));
	slopeVec[1] = 2*fabs(rho0(i,j)-rho0(i,j-1));
	slopeVec[2] = 0.5*fabs(rho0(i,j+1)-rho0(i,j-1));
	slope = 0;
	if(phi)
	  slope = *std::min_element(slopeVec.begin(),slopeVec.end())*sgn(rho0(i,j+1)-rho0(i,j-1));

	fo(i,j) -= vm(i,j)*(rho0(i,j) - 0.5*(1-dt*fabs(vm(i,j))/dx)*slope);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
    	rho0(i,j) += dt/dy*(fi(i,j) - fo(i,j));
      }
  }

  // Lax Wendroff split method ----------------------------g------------------------------------------------------------------

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-lw-",step,".txt"));
    
    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoSplit(i,j) = (1-sqr(u(i,j)*dt/dx))*rho0(i,j) - 0.5*u(i,j)*dt/dx*((1-u(i+1,j)*dt/dx)*rho0(i+1,j)-(1+u(i-1,j)*dt/dx)*rho0(i-1,j));
      }

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rho(i,j) = (1-sqr(v(i,j)*dt/dx))*rhoSplit(i,j) - 0.5*v(i,j)*dt/dx*((1-v(i,j+1)*dt/dx)*rhoSplit(i,j+1)-(1+v(i,j-1)*dt/dx)*rhoSplit(i,j-1));
      }

    rho0 = rho;
  }

  // Kurganov Tadmor semi-discrete (Euler) method ----------------------------------------------------------------------------------------------  

  Matrix rhoxnjk(nx,ny,"rhox");
  Matrix rhoynjk(nx,ny,"rhoy");

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-kt-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoxnjk(i,j) = minmod((rho0(i,j) - rho0(i-1,j))/dx,(rho0(i+1,j) - rho0(i,j))/dx);
	rhoynjk(i,j) = minmod((rho0(i,j) - rho0(i,j-1))/dy,(rho0(i,j+1) - rho0(i,j))/dy);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
	double rhopjp5k = rho0(i+1,j) - 0.5*dx*rhoxnjk(i+1,j);
	double rhomjp5k = rho0(i,j) + 0.5*dx*rhoxnjk(i,j);
	double rhopjm5k = rho0(i,j) - 0.5*dx*rhoxnjk(i,j);
	double rhomjm5k = rho0(i-1,j) + 0.5*dx*rhoxnjk(i-1,j);

	double rhopjkp5 = rho0(i,j+1) - 0.5*dy*rhoynjk(i,j+1);
	double rhomjkp5 = rho0(i,j) + 0.5*dy*rhoynjk(i,j);
	double rhopjkm5 = rho0(i,j) - 0.5*dy*rhoynjk(i,j);
	double rhomjkm5 = rho0(i,j-1) + 0.5*dy*rhoynjk(i,j-1);

	double ujp5k = 0.5*(u(i,j)+u(i+1,j));
	double ujm5k = 0.5*(u(i,j)+u(i-1,j));
	double ajp5k = fabs(ujp5k);
	double ajm5k = fabs(ujm5k);

	double ujkp5 = 0.5*(v(i,j)+v(i,j+1));
	double ujkm5 = 0.5*(v(i,j)+v(i,j-1));
	double ajkp5 = fabs(ujkp5);
	double ajkm5 = fabs(ujkm5);
	
	rho(i,j) = rho0(i,j) 
	  + dt*(-0.5/dx*((ujp5k*rhopjp5k + ujp5k*rhomjp5k) - (ujm5k*rhopjm5k + ujm5k*rhomjm5k)) + 0.5/dx*(ajp5k*(rhopjp5k - rhomjp5k) - ajm5k*(rhopjm5k - rhomjm5k)))
	  + dt*(-0.5/dy*((ujkp5*rhopjkp5 + ujkp5*rhomjkp5) - (ujkm5*rhopjkm5 + ujkm5*rhomjkm5)) + 0.5/dx*(ajkp5*(rhopjkp5 - rhomjkp5) - ajkm5*(rhopjkm5 - rhomjkm5)));
      }
    
    rho0 = rho;
  }

  // Kurganov Tadmor semi-discrete Runge-Kutta 2 method ----------------------------------------------------------------------------------------------  

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-ktrk2-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoxnjk(i,j) = minmod((rho0(i,j) - rho0(i-1,j))/dx,(rho0(i+1,j) - rho0(i,j))/dx);
	rhoynjk(i,j) = minmod((rho0(i,j) - rho0(i,j-1))/dy,(rho0(i,j+1) - rho0(i,j))/dy);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
	double rhopjp5k = rho0(i+1,j) - 0.5*dx*rhoxnjk(i+1,j);
	double rhomjp5k = rho0(i,j) + 0.5*dx*rhoxnjk(i,j);
	double rhopjm5k = rho0(i,j) - 0.5*dx*rhoxnjk(i,j);
	double rhomjm5k = rho0(i-1,j) + 0.5*dx*rhoxnjk(i-1,j);

	double rhopjkp5 = rho0(i,j+1) - 0.5*dy*rhoynjk(i,j+1);
	double rhomjkp5 = rho0(i,j) + 0.5*dy*rhoynjk(i,j);
	double rhopjkm5 = rho0(i,j) - 0.5*dy*rhoynjk(i,j);
	double rhomjkm5 = rho0(i,j-1) + 0.5*dy*rhoynjk(i,j-1);

	double ujp5k = 0.5*(u(i,j)+u(i+1,j));
	double ujm5k = 0.5*(u(i,j)+u(i-1,j));
	double ajp5k = fabs(ujp5k);
	double ajm5k = fabs(ujm5k);

	double ujkp5 = 0.5*(v(i,j)+v(i,j+1));
	double ujkm5 = 0.5*(v(i,j)+v(i,j-1));
	double ajkp5 = fabs(ujkp5);
	double ajkm5 = fabs(ujkm5);
	
	rho(i,j) = rho0(i,j) 
	  + 0.5*dt*(-0.5/dx*((ujp5k*rhopjp5k + ujp5k*rhomjp5k) - (ujm5k*rhopjm5k + ujm5k*rhomjm5k)) + 0.5/dx*(ajp5k*(rhopjp5k - rhomjp5k) - ajm5k*(rhopjm5k - rhomjm5k)))
	  + 0.5*dt*(-0.5/dy*((ujkp5*rhopjkp5 + ujkp5*rhomjkp5) - (ujkm5*rhopjkm5 + ujkm5*rhomjkm5)) + 0.5/dx*(ajkp5*(rhopjkp5 - rhomjkp5) - ajkm5*(rhopjkm5 - rhomjkm5)));
      }

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoxnjk(i,j) = minmod((rho(i,j) - rho(i-1,j))/dx,(rho(i+1,j) - rho(i,j))/dx);
	rhoynjk(i,j) = minmod((rho(i,j) - rho(i,j-1))/dy,(rho(i,j+1) - rho(i,j))/dy);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
	double rhopjp5k = rho(i+1,j) - 0.5*dx*rhoxnjk(i+1,j);
	double rhomjp5k = rho(i,j) + 0.5*dx*rhoxnjk(i,j);
	double rhopjm5k = rho(i,j) - 0.5*dx*rhoxnjk(i,j);
	double rhomjm5k = rho(i-1,j) + 0.5*dx*rhoxnjk(i-1,j);

	double rhopjkp5 = rho(i,j+1) - 0.5*dy*rhoynjk(i,j+1);
	double rhomjkp5 = rho(i,j) + 0.5*dy*rhoynjk(i,j);
	double rhopjkm5 = rho(i,j) - 0.5*dy*rhoynjk(i,j);
	double rhomjkm5 = rho(i,j-1) + 0.5*dy*rhoynjk(i,j-1);

	double ujp5k = 0.5*(u(i,j)+u(i+1,j));
	double ujm5k = 0.5*(u(i,j)+u(i-1,j));
	double ajp5k = fabs(ujp5k);
	double ajm5k = fabs(ujm5k);

	double ujkp5 = 0.5*(v(i,j)+v(i,j+1));
	double ujkm5 = 0.5*(v(i,j)+v(i,j-1));
	double ajkp5 = fabs(ujkp5);
	double ajkm5 = fabs(ujkm5);
	
	rho0(i,j) +=  
	    dt*(-0.5/dx*((ujp5k*rhopjp5k + ujp5k*rhomjp5k) - (ujm5k*rhopjm5k + ujm5k*rhomjm5k)) + 0.5/dx*(ajp5k*(rhopjp5k - rhomjp5k) - ajm5k*(rhopjm5k - rhomjm5k)))
	  + dt*(-0.5/dy*((ujkp5*rhopjkp5 + ujkp5*rhomjkp5) - (ujkm5*rhopjkm5 + ujkm5*rhomjkm5)) + 0.5/dx*(ajkp5*(rhopjkp5 - rhomjkp5) - ajkm5*(rhopjkm5 - rhomjkm5)));
      }
  }

  // Kurganov Tadmor semi-discrete split method ----------------------------------------------------------------------------------------------  

  init(rho0);

  for(int step = 0; step <= nSteps; step++) {

    if(step%10 == 0)
      rho0.write(toString("fields/rho-kts-",step,".txt"));

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoxnjk(i,j) = minmod((rho0(i,j) - rho0(i-1,j))/dx,(rho0(i+1,j) - rho0(i,j))/dx);
      }
    
    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
	double rhopjp5k = rho0(i+1,j) - 0.5*dx*rhoxnjk(i+1,j);
	double rhomjp5k = rho0(i,j) + 0.5*dx*rhoxnjk(i,j);
	double rhopjm5k = rho0(i,j) - 0.5*dx*rhoxnjk(i,j);
	double rhomjm5k = rho0(i-1,j) + 0.5*dx*rhoxnjk(i-1,j);

	double ujp5k = 0.5*(u(i,j)+u(i+1,j));
	double ujm5k = 0.5*(u(i,j)+u(i-1,j));
	double ajp5k = fabs(ujp5k);
	double ajm5k = fabs(ujm5k);

	rho(i,j) = rho0(i,j) 
	  + dt*(-0.5/dx*((ujp5k*rhopjp5k + ujp5k*rhomjp5k) - (ujm5k*rhopjm5k + ujm5k*rhomjm5k)) + 0.5/dx*(ajp5k*(rhopjp5k - rhomjp5k) - ajm5k*(rhopjm5k - rhomjm5k)));
      }

    for(int i=1; i<rho.get_nx()-1; i++) 
      for(int j=1; j<rho.get_ny()-1; j++) {
	rhoynjk(i,j) = minmod((rho(i,j) - rho(i,j-1))/dy,(rho(i,j+1) - rho(i,j))/dy);
      }

    for(int i=2; i<rho.get_nx()-1; i++) 
      for(int j=2; j<rho.get_ny()-1; j++) {
	double rhopjkp5 = rho(i,j+1) - 0.5*dy*rhoynjk(i,j+1);
	double rhomjkp5 = rho(i,j) + 0.5*dy*rhoynjk(i,j);
	double rhopjkm5 = rho(i,j) - 0.5*dy*rhoynjk(i,j);
	double rhomjkm5 = rho(i,j-1) + 0.5*dy*rhoynjk(i,j-1);

	double ujkp5 = 0.5*(v(i,j)+v(i,j+1));
	double ujkm5 = 0.5*(v(i,j)+v(i,j-1));
	double ajkp5 = fabs(ujkp5);
	double ajkm5 = fabs(ujkm5);
	
	rho0(i,j) = rho(i,j) 
	  + dt*(-0.5/dy*((ujkp5*rhopjkp5 + ujkp5*rhomjkp5) - (ujkm5*rhopjkm5 + ujkm5*rhomjkm5)) + 0.5/dx*(ajkp5*(rhopjkp5 - rhomjkp5) - ajkm5*(rhopjkm5 - rhomjkm5)));
      }
  }

  
}


    // for(int i=1; i<rho.get_nx()-1; i++) { 
    //   rho(i,0) = rho(i,rho.get_ny()-2);
    //   rho(i,rho.get_ny()-1) = rho(i,1);
    // }
    
    // for(int j=0; j<rho.get_ny(); j++) {
    //   rho(0,j) = rho(rho.get_nx()-2,j);    
    //   rho(rho.get_nx()-1,j) = rho(1,j);    
    // }
